package rs.aleph.zadatak;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DeletePage {
	private WebDriver driver;
	
	public DeletePage(WebDriver driver){
		this.driver = driver;
	}
	
	public WebElement getModal(){
		return UslovneMetode.waitForElementPresence(driver, By.className("modal-dialog"), 15) ;
	}
	public void confirmDelete() {
		getModal().findElement(By.className("btn-danger")).click();
	}
	
	public void cancelDelete(){
		getModal().findElement(By.className("btn-default")).click();
	}
}


