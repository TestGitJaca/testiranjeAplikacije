package rs.aleph.zadatak;



import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class Nastavnik {
	
	public WebDriver driver;

	public Nastavnik(WebDriver driver) {
		super();
		this.driver = driver;
	}
	public WebElement getNewNastavnicibutton (){
		return UslovneMetode.waitToBeClickable(driver, By.xpath("//button[@ui-sref='nastavnici.new']"), 10);
	}
	public WebElement getModalDialog(){
		return UslovneMetode.waitForElementPresence(driver,By.className("modal-dialog"),10);
	}
	public WebElement getModalTitle() {
		return UslovneMetode.waitForElementPresence(driver, By.id("myNastavniciLabel"), 10);
	}
	public WebElement getName(){
		return UslovneMetode.waitForElementPresence(driver, By.name("ime"), 10);
	}
	
	public void setName(String ime){
		WebElement napisiIme=getName();
		napisiIme.clear();
		napisiIme.sendKeys(ime);
	}
	public WebElement getLastName(){
		return UslovneMetode.waitForElementPresence(driver, By.name("prezime"), 15);
	}
	
	public void setLastName(String prezime){
		WebElement napisiPrezime=getLastName();
		napisiPrezime.clear();
		napisiPrezime.sendKeys(prezime);
	}
	public WebElement getZvanje(){
		return UslovneMetode.waitForElementPresence(driver, By.name("zvanje"), 10);
	}
	
	public void setZvanje(String zvanje){
		WebElement napisiZvanje=getZvanje();
		napisiZvanje.clear();
		napisiZvanje.sendKeys(zvanje);
	}
	public WebElement getCancelButton() {
		return driver.findElement(By.xpath("//span[@translate='entity.action.cancel']"));
	}
	public WebElement getSaveButton(){
		return getModalDialog().findElement(By.className("btn-primary"));
		
	}
	public void createNastavnik( String ime, String prezime, String zvanje) {
		setName(ime);
		setLastName(prezime);
		setZvanje(zvanje);
		getSaveButton().click();
	}
	
	public WebElement getNastavniciTable(){
	    return UslovneMetode.waitForElementPresence(driver, By.className("jh-table"), 10);
		}
		public List<WebElement> getTableRows(){
			return this.getNastavniciTable().findElements(By.tagName("tr"));
		}
		
		public WebElement getNastavniciRowByLastName( String prezime) {
			return UslovneMetode.waitForElementPresence(driver, By.xpath("//*[contains(text(), \"" + prezime + "\")]/../.."), 15);
		}
		
		public void deleteNastavniciByLastName( String prezime){
			getNastavniciRowByLastName(prezime).findElement(By.className("btn-danger")).click();
		}
		
		public void editNastavniciByLastName(String prezime){
			getNastavniciRowByLastName(prezime).findElement(By.className("btn-primary")).click();
			}
		public void editSecondOne(){
			List<WebElement> li = driver.findElements(By.xpath("//button[@ui-sref ='nastavnici.edit({id:nastavnici.id})']"));
			li.get(2).click();
		    
		}
		
		public void viewNastavniciinfo(String prezime){
			getNastavniciRowByLastName(prezime).findElement(By.className("btn-info")).click();
		}
		public boolean isNastavnikInTable(String prezime) {
			return UslovneMetode.isPresent(driver, By.xpath("//*[contains(text(),\"" + prezime + "\")]/../.."));
		}
}
