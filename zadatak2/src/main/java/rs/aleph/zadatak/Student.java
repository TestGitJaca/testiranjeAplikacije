package rs.aleph.zadatak;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;




public class Student {
	private WebDriver driver;

	public Student(WebDriver driver) {
		super();
		this.driver = driver;
	}
	public WebElement getModalDialog(){
		return UslovneMetode.waitForElementPresence(driver,By.className("modal-dialog"),10);
	}
	public WebElement getModalTitle() {
		return UslovneMetode.waitForElementPresence(driver, By.id("myStudentiLabel"), 10);
	}
	public WebElement getIndex(){
		return UslovneMetode.waitForElementPresence(driver, By.name("indeks"), 10);
	}
	
	public void setIndex(String id){
		WebElement index=getIndex();
		index.clear();
		index.sendKeys(id);
	}
	public WebElement getName(){
		return UslovneMetode.waitForElementPresence(driver, By.name("ime"), 10);
	}
	
	public void setName(String ime){
		WebElement napisiIme=getName();
		napisiIme.clear();
		napisiIme.sendKeys(ime);
	}
	public WebElement getLastName(){
		return UslovneMetode.waitForElementPresence(driver, By.name("prezime"), 10);
	}
	
	public void setLastName(String prezime){
		WebElement napisiPrezime=getLastName();
		napisiPrezime.clear();
		napisiPrezime.sendKeys(prezime);
	}
	public WebElement getCity(){
		return UslovneMetode.waitForElementPresence(driver, By.name("grad"), 10);
	}
	
	public void setCity(String grad){
		WebElement napisiGrad=getCity();
		napisiGrad.clear();
		napisiGrad.sendKeys(grad);
	}
	public WebElement getCancelButton() {
		return driver.findElement(By.xpath("//span[@translate='entity.action.cancel']"));
	}
	public WebElement getSaveButton(){
		return getModalDialog().findElement(By.className("btn-primary"));
		
	}
	public void createStudent(String index, String ime, String prezime, String grad) {
		setIndex(index);
		setName(ime);
		setLastName(prezime);
		setCity(grad);
		getSaveButton().click();
	}
	public WebElement getCreateButton(){
	return UslovneMetode.waitToBeClickable(driver, By.xpath(".//button [@ui-sref='studenti.new']"),20);
	}
	public WebElement getStudentsTable(){
    return UslovneMetode.waitForElementPresence(driver, By.className("jh-table"), 10);
	}
	public List<WebElement> getTableRows(){
		return this.getStudentsTable().findElements(By.tagName("tr"));
	}
	
	public WebElement getStudentRowByIndex(String index) {
		return UslovneMetode.waitForElementPresence(driver, By.xpath("//*[contains(text(),\"" + index + "\")]/../.."), 10);
	}
	
	public void deleteStudentByIndex(String index){
		getStudentRowByIndex(index).findElement(By.className("btn-danger")).click();
	}
	
	public void editStudentByIndex(String index){
		getStudentRowByIndex(index).findElement(By.className("btn-primary")).click();
	}
	
	public void viewStudentByIndex(String index){
		getStudentRowByIndex(index).findElement(By.className("btn-info")).click();
	}
	public boolean isStudentInTable(String index) {
		return UslovneMetode.isPresent(driver, By.xpath("//*[contains(text(),\"" + index + "\")]/../.."));
	}
	}
	

