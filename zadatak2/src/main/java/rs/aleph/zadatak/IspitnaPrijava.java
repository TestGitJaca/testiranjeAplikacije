package rs.aleph.zadatak;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;




public class IspitnaPrijava {

	private WebDriver driver;

	public IspitnaPrijava(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public WebElement getModalDialog() {
		return UslovneMetode.waitForElementPresence(driver, By.className("modal-dialog"), 10);
	}

	public WebElement getModalTitle() {
		return UslovneMetode.waitForElementPresence(driver, By.id("myIspitnePrijaveLabel"), 10);
	}

	public WebElement getTeorija() {
		return UslovneMetode.waitForElementPresence(driver, By.id("field_teorija"), 10);
	}

	public void setTeorija(String value) {
		WebElement teorija = this.getTeorija();
		teorija.clear();
		teorija.sendKeys(value);
	}

	public WebElement getZadaci() {
		return UslovneMetode.waitForElementPresence(driver, By.id("field_zadaci"), 10);
	}

	public void setZadaci(String value) {
		WebElement zadaci = this.getZadaci();
		zadaci.clear();
		zadaci.sendKeys(value);
	}

	public Select getIspitniRok() {
		return new Select(UslovneMetode.waitForElementPresence(driver, By.id("field_ispitniRok"), 10));
	}

	public void setIspitniRok(String value) {
		this.getIspitniRok().selectByVisibleText(value);
	}

	public Select getStudent() {
		return new Select(UslovneMetode.waitForElementPresence(driver, By.id("field_student"), 10));
	}

	public void setStudent(String value) {
		this.getStudent().selectByVisibleText(value);
	}

	public Select getPredmet() {
		return new Select(UslovneMetode.waitForElementPresence(driver, By.id("field_predmet"), 10));
	}

	public void setPredmet(String value) {
		this.getPredmet().selectByVisibleText(value);
	}

	public WebElement getCancelBtn() {
		return UslovneMetode.waitToBeClickable(driver, By.className("btn-default"), 10);
	}

	public WebElement getSaveBtn() {
		return getModalDialog().findElement(By.className("btn-primary"));
	}

	public void createIspitnaPrijava(String teorija, String zadaci, String ispitniRok, String student, String predmet) {
		setTeorija(teorija);
		setZadaci(zadaci);
		setIspitniRok(ispitniRok);
		setStudent(student);
		setPredmet(predmet);
		
	}
	public WebElement getCreateBtn() {
		return UslovneMetode.waitToBeClickable(driver, By.xpath("//button [@ui-sref='ispitnePrijave.new']"), 15);
	}

	public WebElement getIspitnaPrijavaTable() {
		return UslovneMetode.waitForElementPresence(driver, By.className("jh-table"), 10);
	}
	public WebElement isIspitnaPrijavaInTable(String student ) {
		return UslovneMetode.waitForElementPresence(driver, By.xpath("//*[contains(text(),\"" + student + "\")]/../.."),10);
	}
	public WebElement getIspitnaPrijavaByStudentName(String student) {
		return UslovneMetode.waitForElementPresence(driver, By.xpath("//*[contains(text(),\"" + student + "\")]/../.."), 10);
	}

	public void deleteIspitnaPrijavaByStudent(String student) {
		getIspitnaPrijavaByStudentName(student).findElement(By.className("btn-danger")).click();
	}

	public void editIspitnaPrijavaByStudent(String student) {
		getIspitnaPrijavaByStudentName(student).findElement(By.className("btn-primary")).click();
	}

	public List<WebElement> getTableRows() {
		return this.getIspitnaPrijavaTable().findElements(By.tagName("tr"));
	}
}




