package rs.aleph.zadatak;
	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;

	public class LogInOut {
		public WebDriver driver;

		public LogInOut(WebDriver driver) {
			this.driver = driver;
		}
		
		public WebElement getUserName(){
			return UslovneMetode.waitForElementPresence(driver, By.id("username"),10);
			
		}
		public void setUserName(String username){
			WebElement user = getUserName();
			user.clear();
			user.sendKeys(username);
			
			}
		public WebElement getPassword(){
			return UslovneMetode.waitForElementPresence(driver, By.id("password"),15);
			
		}
		public void setPassword(String password){
			WebElement pass = getPassword();
			pass.clear();
			pass.sendKeys(password);
		
		}
		public WebElement getButton(){
			return UslovneMetode.waitToBeClickable(driver, By.xpath(".//button[@type='submit']"), 15);
		}
		public String getTextLogin(){
			return UslovneMetode.waitForElementPresence(driver, By.xpath(".//div[@translate='main.logged.message']"), 10).getText();
		}
		public String getFailTextLogin(){
			return UslovneMetode.waitForElementPresence(driver , By.xpath(".//div[@translate='login.messages.error.authentication']/strong"),20).getText();
		}
		 public void login (String username, String password){
			   setUserName(username);
			   setPassword(password);
			   getButton().click();
			   
		   }
		public WebElement getLogOut() {
			return UslovneMetode.waitToBeClickable(driver, By.id("logout"), 10);
		}
		
	}