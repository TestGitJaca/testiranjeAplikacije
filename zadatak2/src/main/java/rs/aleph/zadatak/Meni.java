package rs.aleph.zadatak;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class Meni {
	private WebDriver driver;

	public Meni(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getAccountMenu() {
		return UslovneMetode.waitForElementPresence(driver, By.id("account-menu"), 13);
	}

	public WebElement getSignUp() {
		return UslovneMetode.waitForElementPresence(driver, By.xpath("//a [@ui-sref='login']"), 10);
	}

	public WebElement getEntities() {
		return UslovneMetode.waitToBeClickable(driver, By.xpath(".//span [@translate='global.menu.entities.main']"), 10);
	}

	public WebElement getStudentsLink() {
		return UslovneMetode.waitToBeClickable(driver, By.xpath(".//a [@ui-sref='studenti']"), 10);
	}

	public WebElement getIspitniRokoviLink() {
		return UslovneMetode.waitToBeClickable(driver, By.xpath("//a [@ui-sref='ispitniRokovi']"), 10);
	}

	public WebElement getNastavniciLink() {
		return UslovneMetode.waitToBeClickable(driver, By.xpath("//a [@ui-sref='nastavnici']"), 10);
	}

	public WebElement getPredmetiLink() {
		return UslovneMetode.waitToBeClickable(driver, By.xpath("//a [@ui-sref='predmeti']"), 10);
	}

	public WebElement getIspitnePrijaveLink() {
		return UslovneMetode.waitToBeClickable(driver, By.xpath("//a [@ui-sref='ispitnePrijave']"), 10);
	}

}
