package rs.aleph.zadatak;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Predmet {
	public WebDriver driver;

	public Predmet(WebDriver driver) {
		super();
		this.driver = driver;
	}
	public WebElement getNewPredmetibutton (){
		return UslovneMetode.waitToBeClickable(driver, By.xpath("//button[@ui-sref='predmeti.new']"), 10);
	}
	public WebElement getModalDialog(){
		return UslovneMetode.waitForElementPresence(driver,By.className("modal-dialog"),10);
	}
	public WebElement getModalTitle() {
		return UslovneMetode.waitForElementPresence(driver, By.id("myPredmetiLabel"), 10);
	}
	public WebElement getNaziv(){
		return UslovneMetode.waitForElementPresence(driver, By.name("naziv"), 10);
	}
	
	public void setNaziv(String naziv){
		WebElement napisiNaziv=getNaziv();
		napisiNaziv.clear();
		napisiNaziv.sendKeys(naziv);
	}
	public Select getSelectStudenti(){
		return new Select (UslovneMetode.waitForElementPresence(driver, By.id("field_studenti"), 15));
	}
	
	public void setSelectedStudent(ArrayList <String> students){
		Select studentSelect = getSelectStudenti();
		for(String student : students){
			studentSelect.selectByVisibleText(student);
		}
	}
	public WebElement getCancelButton() {
		return driver.findElement(By.xpath("//span[@translate='entity.action.cancel']"));
	}
	public WebElement getSaveButton(){
		return getModalDialog().findElement(By.className("btn-primary"));
		
	}
	public Select getSelectNastavnici(){
		return new Select(UslovneMetode.waitForElementPresence(driver, By.name("nastavnici"), 10));
	}
	
	public void setSelectNastavnici(ArrayList<String> nastavnici){
		Select selectNastavnik = getSelectNastavnici();
		for(String nastavnik : nastavnici){
			selectNastavnik.selectByVisibleText(nastavnik);
		}
	}
	
	public WebElement getPredmetTable(){
	    return UslovneMetode.waitForElementPresence(driver, By.className("jh-table"), 10);
		}
		public List<WebElement> getTableRows(){
			return this.getPredmetTable().findElements(By.tagName("tr"));
		}
		
		public WebElement getPredmetiNaziv(String naziv) {
			return UslovneMetode.waitForElementPresence(driver, By.xpath("//*[contains(text(), \"" + naziv + "\")]/../.."), 10);
		}
		
		public void deletePredmetiNaziv(String naziv){
			getPredmetiNaziv(naziv).findElement(By.className("btn-danger")).click();
		}
		
		public void editPredmetiNaziv(String naziv){
			getPredmetiNaziv(naziv).findElement(By.className("btn-primary")).click();
		}
	
			
		public boolean isPredmetPresent(String naziv){
			return UslovneMetode.isPresent(driver, By.xpath("//*[contains(text(), \"" + naziv + "\")]/../.."));
		}
		public void Predmetiinfo(String naziv){
			getPredmetiNaziv(naziv).findElement(By.className("btn-info")).click();
			
		}
		public void createPredmet(String naziv, ArrayList studenti, ArrayList nastavnici) {
			setNaziv(naziv);
			setSelectedStudent(studenti);
			setSelectNastavnici(nastavnici);
			getSaveButton().click();
		}
	
}



