package rs.aleph.zadatak;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class IspitniRok {
	private WebDriver driver;

	public IspitniRok(WebDriver driver) {
		super();
		this.driver = driver;
	}

		

		public WebElement getModalDialog() {
			return UslovneMetode.waitForElementPresence(driver, By.className("modal-dialog"), 10);
		}

		public WebElement getModalTitle() {
			return UslovneMetode.waitForElementPresence(driver, By.id("myIspitniRokoviLabel"), 10);
		}

		public WebElement getNaziv() {
			return UslovneMetode.waitForElementPresence(driver, By.name("naziv"), 10);
		}

		public void setNaziv(String value) {
			WebElement el = this.getNaziv();
			el.clear();
			el.sendKeys(value);
		}

		public WebElement getPocetak() {
			return UslovneMetode.waitForElementPresence(driver, By.id("field_pocetak"), 10);
		}

		public void setPocetak(String value) {
			WebElement el = this.getPocetak();
			el.clear();
			el.sendKeys(value);
		}

		public WebElement getKraj() {
			return UslovneMetode.waitForElementPresence(driver, By.id("field_kraj"), 10);
		}

		public void setKraj(String value) {
			WebElement el = this.getKraj();
			el.clear();
			el.sendKeys(value);
		}

		public WebElement getCancelBtn() {
			return UslovneMetode.waitToBeClickable(driver, By.className("btn-default"), 10);
		}

		public WebElement getSaveBtn() {
			return getModalDialog().findElement(By.className("btn-primary"));
		}
		public void createIspitniRok(String naziv, String pocetak, String kraj) {
			setNaziv(naziv);
			setPocetak(pocetak);
			setKraj(kraj);
			getSaveBtn().click();
		}
		public WebElement getCreateButton() {
			return UslovneMetode.waitToBeClickable(driver, By.xpath("//button [@ui-sref=\"ispitniRokovi.new\"]"), 10);
		}

		public WebElement getIspitniRokoviTable() {
			return UslovneMetode.waitForElementPresence(driver, By.className("jh-table"), 10);
		}

		public List<WebElement> getTableRows() {
			return this.getIspitniRokoviTable().findElements(By.tagName("tr"));
		}
		public WebElement getIspitniRokByName(String naziv) {
			return UslovneMetode.waitForElementPresence(driver, By.xpath("//*[contains(text(),\"" + naziv + "\")]/../.."), 15);
		}

		public void deleteIspitniRokByName(String naziv) {
			getIspitniRokByName(naziv).findElement(By.className("btn-danger")).click();
		}

		public void editIspitniRokByName(String naziv) {
			getIspitniRokByName(naziv).findElement(By.className("btn-primary")).click();
		}

		public void viewIspitniRokByName(String naziv) {
			getIspitniRokByName(naziv).findElement(By.className("btn-info")).click();
		}

	
	}

	
	


