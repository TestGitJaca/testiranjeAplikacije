package rs.aleph.zadatak;


import org.testng.annotations.Test;


import org.testng.AssertJUnit;
import org.testng.annotations.BeforeMethod;


public class NastavnikTest extends BaseTest{
	private Meni meni;
	private Nastavnik nastavnik;
	
@BeforeMethod
public void setupPages() {
        meni = new Meni(driver);
       nastavnik=new Nastavnik(driver);
         
	}
@Test
public void UnosNastavnika(){
	meni.getEntities().click();
	AssertJUnit.assertTrue (meni.getNastavniciLink().isDisplayed());
	meni.getNastavniciLink().click();
	UslovneMetode.waitForTitle(driver, "Nastavnicis", 10);
	AssertJUnit.assertEquals("http://localhost:8080/#/nastavnicis", driver.getCurrentUrl());
	AssertJUnit.assertTrue(nastavnik.getNewNastavnicibutton().isDisplayed());
	nastavnik.getNewNastavnicibutton().click();
	AssertJUnit.assertTrue(nastavnik.getName().isDisplayed());
	AssertJUnit.assertTrue(nastavnik.getLastName().isDisplayed());
	AssertJUnit.assertTrue(nastavnik.getZvanje().isDisplayed());
	nastavnik.createNastavnik("Petar", "Petric", "profesor");
	AssertJUnit.assertTrue(nastavnik.isNastavnikInTable("Petric"));
	nastavnik.getNewNastavnicibutton().click();
	nastavnik.createNastavnik("Jasmina", "Matic", "profesor");
   AssertJUnit.assertTrue(nastavnik.isNastavnikInTable("Matic"));
   //ovde nece da uradi edit nastavnika ni delete a ne znam sta je problem pokusala sam na dva nacina sa posebnom metodom editSecondOne i sa editByLastName takodje ne radi ni delete.
   //  nastavnik.editSecondOne(); ili
  // nastavnik.editNastavniciByLastName("Matic");
	//WebElement edit =  nastavnik.getModalDialog();
	//AssertJUnit.assertTrue(edit.isDisplayed());	
	//nastavnik.setLastName("Narancic");
	//nastavnik.getSaveButton().click();
	
//	AssertJUnit.assertTrue(nastavnik.isNastavnikInTable("Narancic"));
	//nastavnik.deleteNastavniciByLastName("Narancic");
 		//WebElement modalDelete = deletePage.getModal();
 		//AssertJUnit.assertTrue(modalDelete.isDisplayed());
 		//deletePage.confirmDelete();
 	//	AssertJUnit.assertFalse(nastavnik.isNastavnikInTable("Narancic"));
 //	AssertJUnit.assertEquals (2,nastavnik.getTableRows().size());

			}
	

}
