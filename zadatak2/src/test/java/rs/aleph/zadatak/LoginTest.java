package rs.aleph.zadatak;

import org.testng.annotations.Test;


import org.testng.AssertJUnit;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;


public class LoginTest extends BaseTest{
	    private LogInOut loginout;
	    private Meni meni;
	    private String baseUrl;
	    
	    

		@BeforeSuite
		public void setupSelenium() {
			baseUrl = "localhost:8080/#/";
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			driver.manage().window().setSize(new Dimension(1024, 768));
			driver.navigate().to(baseUrl);
		}
	@BeforeMethod
	public void setupPages() {
			loginout = new LogInOut(driver);
	        meni = new Meni(driver);
	        
		}
	@Test	
		public void TestLogovanja () throws InterruptedException{
			meni.getAccountMenu().click();
			
			AssertJUnit.assertEquals ("http://localhost:8080/#/", driver.getCurrentUrl());
			AssertJUnit.assertEquals (true, meni.getSignUp().isDisplayed());
			meni.getSignUp().click();
			UslovneMetode.waitForTitle(driver, "Sign in", 10);
			AssertJUnit.assertEquals ("http://localhost:8080/#/login", driver.getCurrentUrl());
			loginout.login(" ", "admin");
			String message="Failed to sign in!";
			//zasto ove asertacije ne rade
			System.out.println(message);
			//AssertJUnit.assertEquals (message, loginout.getFailTextLogin());
			loginout.login("admin", " ");
			//assertEquals (message,loginout.getFailTextLogin());
			loginout.login("admin","admin");
			String expectedMessage = "You are logged in as user \"admin\".";
			AssertJUnit.assertEquals(expectedMessage, loginout.getTextLogin());
	}

}
