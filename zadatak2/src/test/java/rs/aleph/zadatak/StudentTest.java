package rs.aleph.zadatak;

import org.testng.annotations.Test;


import org.testng.AssertJUnit;

import static org.testng.Assert.assertFalse;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;



public class StudentTest extends BaseTest{
	private Meni meni;
	private Student student;
	private DeletePage deletePage;
    

	
@BeforeMethod
public void setupPages() {
        meni = new Meni(driver);
        student=new Student(driver);
       deletePage=new DeletePage(driver);
         
	}
@Test
public void UnosIzmenaBrisanjeStudenta(){
	meni.getEntities().click();
	AssertJUnit.assertEquals ("http://localhost:8080/#/", driver.getCurrentUrl());
	AssertJUnit.assertTrue (meni.getStudentsLink().isDisplayed());
	meni.getStudentsLink().click();
	UslovneMetode.waitForTitle(driver, "Studentis", 10);
	AssertJUnit.assertEquals ("http://localhost:8080/#/studentis", driver.getCurrentUrl());
	AssertJUnit.assertTrue (student.getCreateButton().isDisplayed());
	student.getCreateButton().click();
	AssertJUnit.assertEquals ("http://localhost:8080/#/studentis/new", driver.getCurrentUrl());
	AssertJUnit.assertTrue(student.getModalDialog().isDisplayed());
	AssertJUnit.assertEquals("Create or edit a Studenti", student.getModalTitle().getText());
	AssertJUnit.assertTrue(student.getIndex().isDisplayed());
	AssertJUnit.assertTrue(student.getLastName().isDisplayed());
	AssertJUnit.assertTrue(student.getCity().isDisplayed());
	AssertJUnit.assertTrue(student.getCancelButton().isDisplayed());
	AssertJUnit.assertTrue(student.getSaveButton().isDisplayed());
	student.setIndex("RA432011");
	student.setName("Miki");
	student.setLastName("Mikilic");
	student.setCity("Novi Sad");
	student.getSaveButton().click();
	AssertJUnit.assertNotNull (student.getStudentRowByIndex("RA432011"));
	student.getCreateButton().click();
	AssertJUnit.assertTrue(student.getModalDialog().isDisplayed());
    student.createStudent("RA452011", "Mihajlo", "Testeric", "Novi Sad");
    student.editStudentByIndex("RA452011");	
	WebElement edit = student.getModalDialog();
	AssertJUnit.assertTrue(edit.isDisplayed());	
	student.setCity("Kraljevo");
	student.getSaveButton().click();
	WebElement stRow = student.getStudentRowByIndex("RA452011");
	String rowData = stRow.getText();
	AssertJUnit.assertTrue(rowData.contains("Kraljevo"));

    student.deleteStudentByIndex("RA452011");
 		WebElement modalDelete = deletePage.getModal();
 		AssertJUnit.assertTrue(modalDelete.isDisplayed());
 		deletePage.confirmDelete();
 		assertFalse(student.isStudentInTable("RA452011"));



    
}
}