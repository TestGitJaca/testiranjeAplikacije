package rs.aleph.zadatak;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LogoutTest extends BaseTest{
    private LogInOut loginout;
    private Meni meni;


@BeforeMethod
public void setupPages() {
		loginout = new LogInOut(driver);
        meni = new Meni(driver);
        
	}

	
@Test	

		public void logout() {
			meni.getAccountMenu().click();
			loginout.getLogOut().click();
			
		}
		@AfterSuite
		public void closeSelenium() {
			driver.quit();
		}
		
}
