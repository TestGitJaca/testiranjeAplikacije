package rs.aleph.zadatak;


import static org.testng.AssertJUnit.assertFalse;
import org.openqa.selenium.WebElement;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class IspitniRokTest extends BaseTest{
	
		private Meni meni;
		private IspitniRok ispitniRok;
		
	@BeforeMethod
	public void setupPages() {
	        meni = new Meni(driver);
	        ispitniRok=new IspitniRok(driver);
	         
		}
	@Test
	public void UnosIspitnogRoka(){
		meni.getEntities().click();
		AssertJUnit.assertTrue (meni.getIspitniRokoviLink().isDisplayed());
		AssertJUnit.assertEquals("http://localhost:8080/#/predmetis", driver.getCurrentUrl());
		meni.getIspitniRokoviLink().click();
		UslovneMetode.waitForTitle(driver, "IspitniRokovis", 10);
		AssertJUnit.assertEquals("http://localhost:8080/#/ispitniRokovis", driver.getCurrentUrl());
		AssertJUnit.assertTrue(ispitniRok.getCreateButton().isDisplayed());
		ispitniRok.getCreateButton().click();
		AssertJUnit.assertTrue(ispitniRok.getModalDialog().isDisplayed());
		AssertJUnit.assertTrue(ispitniRok.getNaziv().isDisplayed());
		AssertJUnit.assertTrue(ispitniRok.getPocetak().isDisplayed());
		AssertJUnit.assertTrue(ispitniRok.getKraj().isDisplayed());
		AssertJUnit.assertTrue(ispitniRok.getCancelBtn().isDisplayed());
		AssertJUnit.assertTrue(ispitniRok.getSaveBtn().isDisplayed());
		ispitniRok.createIspitniRok("Septembarski", "2017-09-15","2017-09-25");
		WebElement stRow = ispitniRok.getIspitniRokByName("Septembarski");
		String rowData = stRow.getText();
		AssertJUnit.assertTrue(rowData.contains("Septembarski"));
		ispitniRok.editIspitniRokByName("Septembarski");
		AssertJUnit.assertTrue(ispitniRok.getModalDialog().isDisplayed());
		ispitniRok.setNaziv("SeptembarskiDodatni");
        ispitniRok.getSaveBtn().click();		

	}
	@Test
	public void UnosNeispravnogIspitnogRoka(){
		ispitniRok.getCreateButton().click();
		AssertJUnit.assertTrue(ispitniRok.getModalDialog().isDisplayed());
		AssertJUnit.assertTrue(ispitniRok.getNaziv().isDisplayed());
		AssertJUnit.assertTrue(ispitniRok.getPocetak().isDisplayed());
		AssertJUnit.assertTrue(ispitniRok.getKraj().isDisplayed());
		AssertJUnit.assertTrue(ispitniRok.getCancelBtn().isDisplayed());
		AssertJUnit.assertTrue(ispitniRok.getSaveBtn().isDisplayed());
		ispitniRok.createIspitniRok("Januarski", "2018-01-15","2017-01-25");
		assertFalse (ispitniRok.getCreateButton().isEnabled());	
		//ovaj test treba da padne, jer je datum pocetka veci od datuma kraja
}
}
