package rs.aleph.zadatak;

import org.openqa.selenium.WebElement;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



public class IspitnePrijaveTest extends BaseTest {
	private Meni meni;
	private IspitnaPrijava ispitnaPrijava;
	
@BeforeMethod
public void setupPages() {
        meni = new Meni(driver);
        ispitnaPrijava=new IspitnaPrijava(driver);
         
	}
@Test
public void UnosIspitnePrijave(){
	meni.getEntities().click();
	AssertJUnit.assertTrue (meni.getIspitnePrijaveLink().isDisplayed());
	meni.getIspitnePrijaveLink().click();
	UslovneMetode.waitForTitle(driver, "IspitnePrijaves", 10);
	AssertJUnit.assertEquals("http://localhost:8080/#/ispitnePrijaves", driver.getCurrentUrl());
	AssertJUnit.assertTrue(ispitnaPrijava.getCreateBtn().isDisplayed());
	ispitnaPrijava.getCreateBtn().click();
	AssertJUnit.assertTrue(ispitnaPrijava.getTeorija().isDisplayed());
	AssertJUnit.assertTrue(ispitnaPrijava.getZadaci().isDisplayed());
	AssertJUnit.assertTrue(ispitnaPrijava.getCancelBtn().isDisplayed());
	AssertJUnit.assertTrue(ispitnaPrijava.getSaveBtn().isDisplayed());
	String student ="RA432011 Miki Mikilic";
	ispitnaPrijava.createIspitnaPrijava("60", "90", "SeptembarskiDodatni", student, "Test development");
	ispitnaPrijava.getSaveBtn().click();
	WebElement stRow = ispitnaPrijava.isIspitnaPrijavaInTable(student);
	String rowData = stRow.getText();
	AssertJUnit.assertTrue(rowData.contains(student));
    ispitnaPrijava.editIspitnaPrijavaByStudent(student);
    ispitnaPrijava.setTeorija("95");
    ispitnaPrijava.setZadaci("100");
    ispitnaPrijava.getSaveBtn().click();
  
}
}
