package rs.aleph.zadatak;


import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import java.util.ArrayList;

import org.testng.annotations.BeforeMethod;

public class PredmetiTest extends BaseTest{
	private Meni meni;
	private Predmet predmet;

@BeforeMethod
public void setupPages() {
    meni = new Meni(driver);
   predmet=new Predmet(driver);
   
}
@Test
public void UnosPredmetaNastavnikaStudenta(){
	meni.getEntities().click();
	AssertJUnit.assertTrue (meni.getPredmetiLink().isDisplayed());
	meni.getPredmetiLink().click();
	UslovneMetode.waitForTitle(driver, "Predmetis", 10);
	AssertJUnit.assertEquals("http://localhost:8080/#/predmetis", driver.getCurrentUrl());
	AssertJUnit.assertTrue (predmet.getNewPredmetibutton().isDisplayed());
	predmet.getNewPredmetibutton().click();
	AssertJUnit.assertTrue (predmet.getModalDialog().isDisplayed());
	AssertJUnit.assertTrue (predmet.getNaziv().isDisplayed());
	AssertJUnit.assertTrue (predmet.getSelectStudenti().isMultiple());
	AssertJUnit.assertTrue (predmet.getSelectNastavnici().isMultiple());
	ArrayList<String> studenti = new ArrayList<String>();
	studenti.add ("Miki Mikilic");
	ArrayList<String> nastavnici = new ArrayList<String>();
	nastavnici.add ("Petar Petric");
	predmet.createPredmet("Test development", studenti, nastavnici);
	AssertJUnit.assertTrue(predmet.isPredmetPresent("Test development"));
	
}
}